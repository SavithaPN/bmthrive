﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Common.Dto
{
    public class ContactInformationDto
    {
        public Guid Id { get; set; }
        public Guid SellerId { get; set; }

        public virtual SellerDto Seller { get; set; }
        public string HomePhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string EmailId { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
    }
}
