﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Common.Dto.Constants
{
    public static class BMThriveConstants
    {
        public static class GroupConstants
        {
            public static Guid DefaultRegionalGroupTypeId = Guid.Parse("a07c1f21-7e3d-4878-b3de-82fda902dd17");
            public static Guid DefaultServicesGroupTypeId = Guid.Parse("b5acd688-a14b-4ee3-a7b3-853ce60f2945");
        }

        public static class MemberConstants
        {
            public static Guid DefaultGoldTypeId = Guid.Parse("9656dd90-f684-43ef-9a42-7d9ccdf337a2");
            public static Guid DefaultSilverTypeId = Guid.Parse("bc2070e2-2dfa-4686-b046-2b90e4d6affa");
            public static Guid DefaultBronzeTypeId = Guid.Parse("545233b5-7e6c-493d-a356-5f1edd2e4f9b");
            public static Guid Member = Guid.Parse("9827a63d-df35-4726-af80-59f3a8f9de4e");
            public static Guid Fan = Guid.Parse("e9a637aa-17b6-406c-878d-52b88af6690c");
            
            
        }
    }
}
