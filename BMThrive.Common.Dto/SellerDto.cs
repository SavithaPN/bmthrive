﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Common.Dto
{
    public class SellerDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SellerIdNumber1 { get; set; }
        public string SellerIdNumber2 { get; set; }
        public string LicenseNumber { get; set; }
        public string AdditionalInformation { get; set; }
        public virtual AddressDto Address { get; set; }
        public virtual ContactInformationDto ContactInfo { get; set; }

        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
        public bool IsActive { get; set; }
    }
}
