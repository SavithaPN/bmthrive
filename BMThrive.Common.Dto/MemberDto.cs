﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Common.Dto
{
    public class MemberDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public AddressDto Address { get; set; }
        public string IdInfoPrimary { get; set; }
        public string IdInfoSecondry { get; set; }
        public string IdInfoAddtional { get; set; }
        public string ContactNumber { get; set; }
        public Guid MemberType_Id { get; set; }
        public string Email { get; set; }
        public string AdditionalInfo { get; set; }
        public DateTime RegisteredDate { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
        public Guid CustomerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }

        public MemberMediaDto MemberPhoto { get; set; }
        public MemberTypeDto Type { get; set; }
    }
}
