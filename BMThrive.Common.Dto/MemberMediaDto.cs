﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Common.Dto
{
   public class MemberMediaDto
    {
        public Guid Id { get; set; }
        public Guid MemberId { get; set; }
        //public virtual MemberDto Member { get; set; }
        public byte[] MediaFile { get; set; }
        public string MediaContentType { get; set; }
        public bool Active { get; set; }
        public string MediaFileDownloadUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
    }
}
