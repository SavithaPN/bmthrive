﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Common.Dto
{
    public class OfferingDto
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public virtual ProductDto Product { get; set; }
        public Guid SellerId { get; set; }
        public SellerDto Seller { get; set; }
        public string OfferDescription { get; set; }
        public int NoOfItemsAvailable { get; set; }
        public string PackageFormat { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
    }
}
