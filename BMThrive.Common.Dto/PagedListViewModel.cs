﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Common.Dto
{
    public class PagedListViewModel<T>
    {
        public int TotalItemCount { get; set; }
        public List<T> PageItems { get; set; }
    }
}
