﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Common.Dto
{
    public class ComponentDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<ComponentImagesDto> ComponentImages { get; set; }
        public virtual ICollection<ComponentPropertiesDto> ComponentProperties { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
    }

    public class ComponentImagesDto
    {
        public Guid Id { get; set; }
        public Guid ComponentId { get; set; }
        public ComponentDto Component { get; set; }
        public string Type { get; set; }
        public byte[] Image { get; set; }
        public int SlNo { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
    }

    public class ComponentPropertiesDto
    {
        public Guid Id { get; set; }
        public Guid ComponentId { get; set; }
        public ComponentDto Component { get; set; }
        public string Name { get; set; }
        public string ComponentPropertyType { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
    }
}
