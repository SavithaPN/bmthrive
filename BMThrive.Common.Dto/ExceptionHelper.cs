﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Common.Dto
{
    public class ExceptionHelper
    {
        public string GetFullExceptionString(Exception e)
        {
            string sMessage = e.Message;

            if (e.InnerException != null)
            {
                sMessage += "--" + GetFullExceptionString(e.InnerException);
            }

            return sMessage;
        }
    }
}
