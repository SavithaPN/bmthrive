﻿using BMThrive.Domain.Entities;
using BMThrive.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Domain.Entities
{
    public class Address :IBasicTrackable
    {
        [Key, ForeignKey("Member")]
        public Guid Id { get; set; }
               
        public Guid? MemberId { get; set; }
        //[Required]
        public virtual Member Member { get; set; }
        public Guid? SellerId { get; set; }
        //[Required]
        public virtual Seller Seller { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public string District { get; set; }
        public string WardNumber { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get ; set ; }
        public DateTime LastAction { get ; set; }
        public Guid LastActionBy { get; set ; }
    }
}
