﻿using BMThrive.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Domain.Entities
{
    public class ProductProperties : IBasicTrackable
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public Product Product { get; set; }
        public string Value { get; set; }
        public string ProductPropertyName { get; set; }
        public DateTime CreatedDate { get ; set; }
        public Guid CreatedBy { get; set ; }
        public DateTime LastAction { get ; set; }
        public Guid LastActionBy { get ; set; }
        public bool IsActive { get; set; } = true;
    }
}
