﻿using BMThrive.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Domain.Entities
{
    public class Product : IBasicTrackable
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public CategoryType Category { get; set; }

        public virtual ICollection<ProductImages> ProductImages { get; set; }
        public virtual ICollection<ProductProperties> ProductProperties { get; set; }

        public Guid SellerId { get; set; }
        public virtual Seller Seller { get; set; }
        public string Description { get; set; }
        public string Unit { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }

        public bool IsActive { get; set; } = true;
    }
}
