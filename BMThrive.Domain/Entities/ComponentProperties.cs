﻿using BMThrive.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Domain.Entities
{
    public class ComponentProperties : IBasicTrackable
    {
        public Guid Id { get; set; }
        public Guid ComponentId { get; set; }
        public Component Component { get; set; }
        public string Name { get; set; }
        public string ComponentPropertyType { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
        public bool IsActive { get; set; } = true;
    }
}
