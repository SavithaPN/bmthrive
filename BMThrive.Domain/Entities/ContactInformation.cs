﻿using BMThrive.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Domain.Entities
{
    public class ContactInformation : IBasicTrackable
    {
        [Key, ForeignKey("Seller")]
        public Guid Id { get; set; }

        public Guid SellerId { get; set; }
        [Required]
        public virtual Seller Seller { get; set; }
        public string HomePhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string EmailId { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
    }
}
