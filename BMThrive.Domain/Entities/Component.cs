﻿using BMThrive.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Domain.Entities
{
    public class Component :IBasicTrackable
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<ComponentImages> ComponentImages { get; set; }
        public virtual ICollection<ComponentProperties> ComponentProperties { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; } = true;

        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
    }
}
