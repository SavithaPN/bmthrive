﻿using BMThrive.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Domain.Entities
{
    public class Offering : IBasicTrackable
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public virtual Product Product { get; set; }
        public Guid SellerId { get; set; }
        public Seller Seller { get; set; }
        public string OfferDescription { get; set; }
        public int NoOfItemsAvailable { get; set; }
        public string PackageFormat { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
        public bool IsActive { get; set; } = true;
    }
}
