﻿using BMThrive.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.Domain.Entities
{
    public class MemberMedia : IBasicTrackable
    {
        [Key, ForeignKey("Member")]
        public Guid Id { get; set; }
        public Guid MemberId { get; set; }
        [Required]
        public virtual Member Member { get; set; }
        public byte[] MediaFile { get; set; }
        public string MediaContentType { get; set; }
        public bool Active { get; set; } = true;
        public string MediaFileDownloadUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
    }
}
