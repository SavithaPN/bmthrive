﻿using BMThrive.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BMThrive.Domain.Entities
{
    public class Member :IBasicTrackable
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        [Required]
        public virtual Address Address { get; set; }
        public string IdInfoPrimary { get; set; }
        public string IdInfoSecondry { get; set; }
        public string IdInfoAddtional { get; set; }
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public string AdditionalInfo { get; set; }
        public DateTime RegisteredDate { get; set; }
        public Guid MemberType_Id { get; set; }
        public DateTime? MembershipExpiry { get; set; }
        //public MemberType MemberType { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; } = true;
        public Guid CustomerId { get; set; }
        public DateTime CreatedDate { get ; set; }
        public Guid CreatedBy { get ; set ; }
        public DateTime LastAction { get ; set; }
        public Guid LastActionBy { get ; set ; }
        public MemberMedia MemberPhoto { get; set; }

    }
}
