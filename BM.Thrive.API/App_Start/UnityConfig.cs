using AutoMapper;
using BM.Thrive.API.Helpers;
using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.CommonLogic.Services;
using BMThrive.DataAccess.Contexts;
using BMThrive.DataAccess.Repository;
using BMThrive.DataAccess.Repository.Interfaces;
using System;
using System.Data.Entity;
using Unity;
using Unity.Injection;

namespace BM.Thrive.API
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<BMThriveDBContext>(new InjectionConstructor("BMThriveDBContext"));
            container.RegisterType<DbContext, BMThriveDBContext>();
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType<IMapper, Mapper>();
            container.RegisterType<IApiAuthControllerHelper, ApiAuthControllerHelper>();
            container.RegisterType<IApiAuthService, ApiAuthService>();
            container.RegisterType<ICustomerControllerHelper, CustomerControllerHelper>();
            container.RegisterType<ICustomerService, CustomerService>();
            container.RegisterType<IMemberControllerHelper, MemberControllerHelper>();
            container.RegisterType<IMemberService, MemberService>();
            container.RegisterType<IGroupService, GroupService>();
            container.RegisterType<IGroupControllerHelper, GroupControllerHelper>();
            container.RegisterType<IComponentService, ComponentService>();
            container.RegisterType<IOfferingService, OfferingService>();
            container.RegisterType<IProductService, ProductService>();
            container.RegisterType<ISellerService, SellerService>();
            container.RegisterType<IComponentControllerHelper, ComponentControllerHelper>();
            container.RegisterType<IOfferingControllerHelper, OfferingControllerHelper>();
            container.RegisterType<IProductControllerHelper, ProductControllerHelper>();
            container.RegisterType<ISellerControllerHelper, SellerControllerHelper>();
            
            //container.RegisterType<IUnitOfWork, UnitOfWork>();
        }
    }
}