﻿using AutoMapper;
using BMThrive.Common.Dto;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BM.Thrive.API.Mappings
{
    public class MemberMediaMappingProfile: Profile
    {
        public MemberMediaMappingProfile()
        {
            CreateMap<MemberMediaDto, MemberMedia>().ReverseMap();
        }
    }
}