﻿using AutoMapper;
using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BM.Thrive.API.Helpers
{
    public class ComponentControllerHelper : IComponentControllerHelper
    {
        IComponentService _componentService;
        IMapper _mapperDtoToEntity;
        IMapper _mapperEntityToDto;
        public ComponentControllerHelper(IComponentService componentService)
        {
            _componentService = componentService;
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<ComponentDto, Component>();
                cfg.CreateMap<ComponentImagesDto, ComponentImages>();
                cfg.CreateMap<ComponentPropertiesDto, ComponentProperties>();
            });
            _mapperDtoToEntity = config.CreateMapper();

            config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Component, ComponentDto>();
                cfg.CreateMap<ComponentImages, ComponentImagesDto>();
                cfg.CreateMap<ComponentProperties, ComponentPropertiesDto>();
            });
            _mapperEntityToDto = config.CreateMapper();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddComponentPicture(ComponentImagesDto componentImage)
        {
            ComponentImages comImages = _mapperDtoToEntity.Map<ComponentImages>(componentImage);
            var result = await _componentService.AddComponentPicture(comImages);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddComponentProperty(ComponentPropertiesDto componentProperty)
        {
            ComponentProperties comProperties = _mapperDtoToEntity.Map<ComponentProperties>(componentProperty);
            var result = await _componentService.AddComponentProperty(comProperties);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateComponentFull(ComponentDto component)
        {
            Component componentData = _mapperDtoToEntity.Map<Component>(component);
            var result = await _componentService.CreateComponentFull(componentData);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateComponentLite(ComponentDto component)
        {
            Component componentData = _mapperDtoToEntity.Map<Component>(component);
            var result = await _componentService.CreateComponentLite(componentData);
            return result;
        }

        public async Task<ComponentDto> GetComponent(Guid id)
        {
            var component = await _componentService.GetComponent(id);
            ComponentDto componentDto = _mapperEntityToDto.Map<ComponentDto>(component);
            return componentDto;
        }

        public async Task<List<ComponentImagesDto>> GetComponentImages(Guid id)
        {
            var componentImages = await _componentService.GetComponentImages(id);
            List<ComponentImagesDto> componentImageDtos = _mapperEntityToDto.Map<List<ComponentImagesDto>>(componentImages);
            return componentImageDtos;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemovePicture(Guid imageId)
        {
            var result = await _componentService.RemovePicture(imageId);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProperty(Guid propertyId)
        {
            var result = await _componentService.RemoveProperty(propertyId);
            return result;
        }

        public async Task<List<ComponentDto>> SearchComponent(string searchText = "")
        {
            List<Component> components = await _componentService.SearchComponent(searchText);
            List<ComponentDto> componentDtos = _mapperEntityToDto.Map<List<ComponentDto>>(components);
            return componentDtos;
        }
    }
}