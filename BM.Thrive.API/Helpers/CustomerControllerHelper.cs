﻿using AutoMapper;
using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BM.Thrive.API.Helpers
{
    public class CustomerControllerHelper : ICustomerControllerHelper
    {
        ICustomerService _customerService;
        IMapper _mapper;

        public CustomerControllerHelper(
            ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddCustomer(CustomerDto customerDto)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<CustomerDto,Customer >();
            });
            _mapper = config.CreateMapper();
            Customer customer = _mapper.Map<Customer>(customerDto);
            return await _customerService.AddCustomer(customer);
        }

        public async Task<IEnumerable<CustomerDto>> ListCustomers()
        {
            var customers = await _customerService.ListCustomers();
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Customer, CustomerDto>();
            });
            _mapper = config.CreateMapper();
            List<CustomerDto> customerDtos = _mapper.Map<IEnumerable<CustomerDto>>(customers).ToList();
            return customerDtos;
        }
    }
}