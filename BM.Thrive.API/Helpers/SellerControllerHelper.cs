﻿using AutoMapper;
using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BM.Thrive.API.Helpers
{
    public class SellerControllerHelper : ISellerControllerHelper
    {
        ISellerService _sellerService;
        IMapper _mapperEntityToDto;
        IMapper _mapperDtoToEntity;

        public SellerControllerHelper(ISellerService sellerService)
        {
            _sellerService = sellerService;
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Seller, SellerDto>();
                cfg.CreateMap<ContactInformation, ContactInformationDto>();
                cfg.CreateMap<Address, AddressDto>();
            });

            _mapperEntityToDto = config.CreateMapper();

            config = new MapperConfiguration(cfg => {
                cfg.CreateMap<SellerDto, Seller>();
                cfg.CreateMap<ContactInformationDto, ContactInformation>();
                cfg.CreateMap<AddressDto, Address>();
            });

            _mapperDtoToEntity = config.CreateMapper();
        }
        
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateSeller(SellerDto sellerDto)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            Seller seller = _mapperDtoToEntity.Map<Seller>(sellerDto);
            if (seller == null)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ValidationFailed);
                result.Errors.Add("Input data is null");

            }
            if (seller.Id == null || seller.Id == Guid.Empty)
            {
                seller.Id = Guid.NewGuid();
            }
            seller.CreatedDate = DateTime.UtcNow;
            seller.LastAction = DateTime.UtcNow;

            result = await _sellerService.CreateSeller(seller);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteSeller(Guid id)
        {
            var result = await _sellerService.DeleteSeller(id);
            return result;
        }

        public async Task<SellerDto> GetSeller(Guid id)
        {
            var seller = await _sellerService.GetSeller(id);
            SellerDto sellerDto = _mapperEntityToDto.Map<SellerDto>(seller);
            return sellerDto;
        }

        public async Task<List<SellerDto>> SearchSeller(string address = "", string name = "")
        {
            var sellers = await _sellerService.SearchSeller(address, name);
            List<SellerDto> sellerDtos = _mapperEntityToDto.Map<List<SellerDto>>(sellers);
            return sellerDtos;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateSeller(SellerDto sellerDto)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            Seller seller = _mapperDtoToEntity.Map<Seller>(sellerDto);
            if (seller == null)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ValidationFailed);
                result.Errors.Add("Input data is null");

            }
            
            seller.LastAction = DateTime.UtcNow;
            result = await _sellerService.UpdateSeller(seller);
            return result;
        }
    }
}