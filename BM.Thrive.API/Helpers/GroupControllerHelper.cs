﻿using AutoMapper;
using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BM.Thrive.API.Helpers
{
    public class GroupControllerHelper : IGroupControllerHelper
    {
        IGroupService _groupService;
        IMapper _mapperDtoToEntity;
        IMapper _mapperEntityToDto;

        public GroupControllerHelper(IGroupService groupService)
        {
            _groupService = groupService;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Group, GroupDto>();
                cfg.CreateMap<GroupType, GroupTypeDto>();
                cfg.CreateMap<MemberGroupMap, MemberGroupMapDto>();
            });

            _mapperEntityToDto = config.CreateMapper();

            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<GroupDto, Group>();
                cfg.CreateMap<GroupTypeDto, GroupType>();
                cfg.CreateMap<MemberGroupMapDto, MemberGroupMap>();
            });

            _mapperDtoToEntity = config.CreateMapper();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddGroup(GroupDto groupDto)
        {
            Group group = _mapperDtoToEntity.Map<Group>(groupDto);
            var result = await _groupService.AddGroup(group);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddMemberGroupMap(MemberGroupMapDto memberGroupMapDto)
        {
            MemberGroupMap memberGroupMap = _mapperDtoToEntity.Map<MemberGroupMap>(memberGroupMapDto);
            var result = await _groupService.AddMemberGroupMap(memberGroupMap);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteGroup(Guid groupId)
        {
            var result = await _groupService.DeleteGroup(groupId);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteMemberGroupMap(Guid memberGroupMapId)
        {
            var result = await _groupService.DeleteMemberGroupMap(memberGroupMapId);
            return result;
        }

        public async Task<List<GroupTypeDto>> GetGroupTypes()
        {
            var groupTypes = await _groupService.GetGroupTypes();
            List<GroupTypeDto> groupTypeDtos = _mapperEntityToDto.Map<List<GroupTypeDto>>(groupTypes);
            return groupTypeDtos;
        }

        public async Task<List<GroupDto>> ListAllGroups()
        {
            var groupDtos = await _groupService.ListAllGroups();
            List<GroupDto> groups = _mapperEntityToDto.Map<List<GroupDto>>(groupDtos);
            return groups;
        }

        public async Task<List<GroupDto>> ListGroups(Guid customerId)
        {
            var groupDtos = await _groupService.ListGroups(customerId);
            List<GroupDto> groups = _mapperEntityToDto.Map<List<GroupDto>>(groupDtos);
            return groups;
        }
    }
}