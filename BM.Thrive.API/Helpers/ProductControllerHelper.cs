﻿using AutoMapper;
using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BM.Thrive.API.Helpers
{
    public class ProductControllerHelper : IProductControllerHelper
    {
        IProductService _productService;
        IMapper _mapperDtoToEntity;
        IMapper _mapperEntityToDto;

        public ProductControllerHelper(IProductService productService)
        {
            _productService = productService;

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<ProductDto, Product>();
                cfg.CreateMap<ProductPropertiesDto, ProductProperties>();
                cfg.CreateMap<ProductImagesDto, ProductImages>();
                cfg.CreateMap<CategoryTypeDto, CategoryType>();

            });

            _mapperDtoToEntity = config.CreateMapper();

            config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Product, ProductDto>();
                cfg.CreateMap<ProductProperties, ProductPropertiesDto>();
                cfg.CreateMap<ProductImages, ProductImagesDto>();
                cfg.CreateMap<CategoryType, CategoryTypeDto>();
            });

            _mapperEntityToDto = config.CreateMapper();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddProductPicture(ProductImagesDto productImageDto)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            if(productImageDto == null)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ValidationFailed);
                result.Errors.Add("Input data is null");
                return result;
            }

            ProductImages productImage = _mapperDtoToEntity.Map<ProductImages>(productImageDto);
            if(productImage.Id == null || productImage.Id == Guid.Empty)
            {
                productImage.Id = Guid.NewGuid();
            }

            productImage.CreatedDate = DateTime.UtcNow;
            productImage.LastAction = DateTime.UtcNow;

            result = await _productService.AddProductPicture(productImage);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddProductProperties(ProductPropertiesDto productPropertiesDto)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            if (productPropertiesDto == null)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ValidationFailed);
                result.Errors.Add("Input data is null");
                return result;
            }

            ProductProperties productProperties = _mapperDtoToEntity.Map<ProductProperties>(productPropertiesDto);
            if (productProperties.Id == null || productProperties.Id == Guid.Empty)
            {
                productProperties.Id = Guid.NewGuid();
            }

            productProperties.CreatedDate = DateTime.UtcNow;
            productProperties.LastAction = DateTime.UtcNow;

            result = await _productService.AddProductProperties(productProperties);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateProductFull(ProductDto productDto)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            if (productDto == null)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ValidationFailed);
                result.Errors.Add("Input data is null");
                return result;
            }

            Product product = _mapperDtoToEntity.Map<Product>(productDto);
            if (product.Id == null || product.Id == Guid.Empty)
            {
                product.Id = Guid.NewGuid();
            }

            product.CreatedDate = DateTime.UtcNow;
            product.LastAction = DateTime.UtcNow;

            if(product.ProductProperties != null && product.ProductProperties.Count > 0)
            {
                foreach(var pProperty in product.ProductProperties)
                {
                    if (pProperty.Id == null || pProperty.Id == Guid.Empty)
                    {
                        pProperty.Id = Guid.NewGuid();
                    }

                    pProperty.CreatedDate = DateTime.UtcNow;
                    pProperty.LastAction = DateTime.UtcNow;
                }
            }

            if(product.ProductImages != null && product.ProductImages.Count > 0)
            {
                foreach (var pImage in product.ProductImages)
                {
                    if (pImage.Id == null || pImage.Id == Guid.Empty)
                    {
                        pImage.Id = Guid.NewGuid();
                    }

                    pImage.CreatedDate = DateTime.UtcNow;
                    pImage.LastAction = DateTime.UtcNow;
                }
            }

            result = await _productService.CreateProductFull(product);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateProductLite(ProductDto productDto)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            if (productDto == null)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ValidationFailed);
                result.Errors.Add("Input data is null");
                return result;
            }

            Product product = _mapperDtoToEntity.Map<Product>(productDto);
            if (product.Id == null || product.Id == Guid.Empty)
            {
                product.Id = Guid.NewGuid();
            }

            product.CreatedDate = DateTime.UtcNow;
            product.LastAction = DateTime.UtcNow;

            result = await _productService.CreateProductLite(product);
            return result;
        }

        public async Task<List<ProductImagesDto>> GetProductPicture(Guid productId)
        {
            var productImages = await _productService.GetProductPicture(productId);
            List<ProductImagesDto> productImagesDto = _mapperEntityToDto.Map<List<ProductImagesDto>>(productImages);
            return productImagesDto;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemovePicture(Guid imageId)
        {
            var result = await _productService.RemovePicture(imageId);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProductPictures(Guid productId)
        {
            var result = await _productService.RemoveProductPictures(productId);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProperty(Guid propertyId)
        {
            var result = await _productService.RemoveProperty(propertyId);
            return result;
        }

        public async Task<List<ProductDto>> SearchProduct(string id = "", string name = "", string anyPropertyName = "", string anyPropertyValue = "")
        {
            List<Product> products = await _productService.SearchProduct(id, name, anyPropertyName, anyPropertyValue);
            List<ProductDto> productDtos = _mapperEntityToDto.Map<List<ProductDto>>(products);
            return productDtos;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateProduct(ProductDto productDto)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            if (productDto == null)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ValidationFailed);
                result.Errors.Add("Input data is null");
                return result;
            }

            Product product = _mapperDtoToEntity.Map<Product>(productDto);
            product.LastAction = DateTime.UtcNow;

            result = await _productService.UpdateProduct(product);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateProductProperty(ProductPropertiesDto productPropertiesDto)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            if (productPropertiesDto == null)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ValidationFailed);
                result.Errors.Add("Input data is null");
                return result;
            }

            ProductProperties productProperty = _mapperDtoToEntity.Map<ProductProperties>(productPropertiesDto);
            productProperty.LastAction = DateTime.UtcNow;

            result = await _productService.UpdateProductProperty(productProperty);
            return result;
        }
    }
}