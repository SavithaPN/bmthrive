﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM.Thrive.API.Helpers.Interfaces
{
    public interface IOfferingControllerHelper
    {
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddOffer(OfferingDto offerDto);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateOffer(OfferingDto offerDto);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteOffer(Guid id);
        Task<List<OfferingDto>> SearchOfferings(string searchText = "", bool includeExpiredOffer = false);
    }
}
