﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM.Thrive.API.Helpers.Interfaces
{
    public interface IMemberControllerHelper
    {
        Task<WorkResultDto<Guid, GeneralWorkStatus>> RegisterMember(MemberDto memberDto);
        Task<List<MemberDto>> ListMembersBasedOnCustomer(Guid customerId);
        Task<List<MemberDto>> ListAllMembers();
        Task<PagedListViewModel<MemberDto>> ListMembersSorted(Guid customerId, string sortBy = "", string search="", int count = -1, int skip = -1);
        Task<MemberDto> GetMemberDetails(Guid id);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateMemberStatus(MemberDto memberDto);
        Task<List<MemberTypeDto>> ListMemberTypes();
        Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteMember(Guid memberId);

        Task<List<MemberDto>> ListAllMemberWithOutMedia();
        Task<List<MemberDto>> ListMembersBasedOnCustomerWithoutMedia(Guid customerId);
        Task<PagedListViewModel<MemberDto>> ListMembersSortedWithOutMedia(Guid customerId, string sortBy = "", string search = "", int count = -1, int skip = -1);

        Task<MemberMediaDto> GetMemberMedia(Guid memberId);
    }
}
