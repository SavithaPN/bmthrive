﻿using BMThrive.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM.Thrive.API.Helpers.Interfaces
{
    public interface IApiAuthControllerHelper
    {
        AuthResponseDto Authenticate(AuthInformationDto user);
    }
}
