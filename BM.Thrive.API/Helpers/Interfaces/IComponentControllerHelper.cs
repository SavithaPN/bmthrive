﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM.Thrive.API.Helpers.Interfaces
{
    public interface IComponentControllerHelper
    {
        Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateComponentFull(ComponentDto component);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateComponentLite(ComponentDto component);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddComponentProperty(ComponentPropertiesDto componentProperty);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddComponentPicture(ComponentImagesDto componentImage);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> RemovePicture(Guid imageId);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProperty(Guid propertyId);
        Task<ComponentDto> GetComponent(Guid id);
        Task<List<ComponentDto>> SearchComponent(string searchText = "");
        Task<List<ComponentImagesDto>> GetComponentImages(Guid id);
    }
}
