﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM.Thrive.API.Helpers.Interfaces
{
    public interface ISellerControllerHelper
    {
        Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateSeller(SellerDto seller);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateSeller(SellerDto seller);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteSeller(Guid id);
        Task<SellerDto> GetSeller(Guid id);
        Task<List<SellerDto>> SearchSeller(string address = "", string name = "");

    }
}
