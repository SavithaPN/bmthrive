﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM.Thrive.API.Helpers.Interfaces
{
    public interface IGroupControllerHelper 
    {
        Task<List<GroupDto>> ListGroups(Guid customerId);
        Task<List<GroupDto>> ListAllGroups();
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddGroup(GroupDto group);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteGroup(Guid groupId);

        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddMemberGroupMap(MemberGroupMapDto memberGroupMap);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteMemberGroupMap(Guid memberGroupMapId);

        Task<List<GroupTypeDto>> GetGroupTypes();
    }
}
