﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BM.Thrive.API.Helpers.Interfaces
{
    public interface IProductControllerHelper
    {
        Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateProductFull(ProductDto product);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateProductLite(ProductDto product);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddProductProperties(ProductPropertiesDto productProperties);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddProductPicture(ProductImagesDto productImages);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProductPictures(Guid productId);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> RemovePicture(Guid imageId);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProperty(Guid propertyId);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateProduct(ProductDto product);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateProductProperty(ProductPropertiesDto productProperties);
        Task<List<ProductDto>> SearchProduct(string id = "", string name = "", string anyPropertyName = "", string anyPropertyValue = "");
        Task<List<ProductImagesDto>> GetProductPicture(Guid productId);
    }
}
