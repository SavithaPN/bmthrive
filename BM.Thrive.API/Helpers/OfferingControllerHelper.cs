﻿using AutoMapper;
using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BM.Thrive.API.Helpers
{
    public class OfferingControllerHelper : IOfferingControllerHelper
    {
        IOfferingService _offeringService;
        IMapper _mapperDtoToEntity;
        IMapper _mapperEntityToDto;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offeringService"></param>
        public OfferingControllerHelper(IOfferingService offeringService)
        {
            _offeringService = offeringService;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<OfferingDto, Offering>();
                cfg.CreateMap<ProductDto, Product>();
                cfg.CreateMap<SellerDto, Seller>();
            });

            _mapperDtoToEntity = config.CreateMapper();

            config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Offering, OfferingDto>();
                cfg.CreateMap<Seller, SellerDto>();
                cfg.CreateMap<Product, ProductDto>();
            });

            _mapperEntityToDto = config.CreateMapper();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerDto"></param>
        /// <returns></returns>
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddOffer(OfferingDto offerDto)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            Offering offer = _mapperDtoToEntity.Map<Offering>(offerDto);
            if(offer == null)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ValidationFailed);
                result.Errors.Add("Input data is null");

            }
            if(offer.Id == null || offer.Id == Guid.Empty)
            {
                offer.Id = Guid.NewGuid();
            }
            offer.CreatedDate = DateTime.UtcNow;
            offer.LastAction = DateTime.UtcNow;
          
            result = await _offeringService.AddOffer(offer);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteOffer(Guid id)
        {
            var result = await  _offeringService.DeleteOffer(id);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchText"></param>
        /// <param name="includeExpiredOffer"></param>
        /// <returns></returns>
        public async Task<List<OfferingDto>> SearchOfferings(string searchText = "", bool includeExpiredOffer = false)
        {
            var offerings = await _offeringService.SearchOfferings(searchText, includeExpiredOffer);
            List<OfferingDto> offeringDtos = _mapperEntityToDto.Map<List<OfferingDto>>(offerings);
            return offeringDtos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="offerDto"></param>
        /// <returns></returns>
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateOffer(OfferingDto offerDto)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            Offering offer = _mapperDtoToEntity.Map<Offering>(offerDto);
            if (offer == null)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ValidationFailed);
                result.Errors.Add("Input data is null");

            }
            
            offer.LastAction = DateTime.UtcNow;
            result = await _offeringService.UpdateOffer(offer);
            return result;
        }
    }
}