﻿using AutoMapper;
using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.Domain;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BM.Thrive.API.Helpers
{
    public class MemberControllerHelper : IMemberControllerHelper
    {
        IMemberService _memberService;
        IMapper _mapperDtoToEntity;
        IMapper _mapperEntityToDto;

        public MemberControllerHelper(IMemberService memberService)
        {
            _memberService = memberService;

            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Member, MemberDto>();
                cfg.CreateMap<Address, AddressDto>();
                cfg.CreateMap<MemberType, MemberTypeDto>();
                cfg.CreateMap<MemberMedia, MemberMediaDto>();
            });

            _mapperEntityToDto = config.CreateMapper();

            config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MemberDto, Member>();
                cfg.CreateMap<AddressDto, Address>();
                cfg.CreateMap<MemberTypeDto, MemberType>();
                cfg.CreateMap<MemberMediaDto, MemberMedia>();
            });

            _mapperDtoToEntity = config.CreateMapper();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteMember(Guid memberId)
        {
            return await _memberService.DeleteMember(memberId);
        }

        public async Task<MemberDto> GetMemberDetails(Guid id)
        {
            var member = await _memberService.GetMemberDetails(id);
            MemberDto memberDto = _mapperEntityToDto.Map<MemberDto>(member);
            return memberDto;
        }

        public async Task<MemberMediaDto> GetMemberMedia(Guid memberId)
        {
            var memberMedia = await _memberService.GetMemberMedia(memberId);
            MemberMediaDto memberMediaDto = _mapperEntityToDto.Map<MemberMediaDto>(memberMedia);
            return memberMediaDto;
        }

        public async Task<List<MemberDto>> ListAllMembers()
        {
            var members = await _memberService.ListAllMembers();
            List<MemberDto> membersDto = _mapperEntityToDto.Map<List<MemberDto>>(members);
            return membersDto;
        }

        public async Task<List<MemberDto>> ListAllMemberWithOutMedia()
        {
            var members = await _memberService.ListAllMembers();
            List<MemberDto> membersDto = _mapperEntityToDto.Map<List<MemberDto>>(members);
            return membersDto;
        }

        public async Task<List<MemberDto>> ListMembersBasedOnCustomer(Guid customerId)
        {
            var members = await _memberService.ListMembersBasedOnCustomer(customerId);
            List<MemberDto> membersDto = _mapperEntityToDto.Map<List<MemberDto>>(members);
            return membersDto;
        }

        public async Task<List<MemberDto>> ListMembersBasedOnCustomerWithoutMedia(Guid customerId)
        {
            var members = await _memberService.ListMembersBasedOnCustomer(customerId);
            List<MemberDto> membersDto = _mapperEntityToDto.Map<List<MemberDto>>(members);
            return membersDto;
        }

        public async Task<PagedListViewModel<MemberDto>> ListMembersSorted(Guid customerId, string sortBy = "", string search = "", int count = -1, int skip = -1)
        {
            PagedListViewModel<MemberDto> membersDtoPL = new PagedListViewModel<MemberDto>();
            var members = await _memberService.ListMembersSorted(customerId, sortBy, search, count, skip);
            if (members != null && members.PageItems != null)
            {
                membersDtoPL.TotalItemCount = members.TotalItemCount;
                membersDtoPL.PageItems = _mapperEntityToDto.Map<List<MemberDto>>(members.PageItems);
            }

            return membersDtoPL;
        }

        public async Task<PagedListViewModel<MemberDto>> ListMembersSortedWithOutMedia(Guid customerId, string sortBy = "", string search = "", int count = -1, int skip = -1)
        {
            PagedListViewModel<MemberDto> membersDtoPL = new PagedListViewModel<MemberDto>();
            var members = await _memberService.ListMembersSorted(customerId, sortBy, search, count, skip);
            if (members != null && members.PageItems != null)
            {
                membersDtoPL.TotalItemCount = members.TotalItemCount;
                membersDtoPL.PageItems = _mapperEntityToDto.Map<List<MemberDto>>(members.PageItems);
            }

            return membersDtoPL;
        }

        /// <summary>
        /// List all the member types available
        /// </summary>
        /// <returns></returns>
        public async Task<List<MemberTypeDto>> ListMemberTypes()
        {
            var memberTypes = await _memberService.ListMemberTypes();
            List<MemberTypeDto> memberTypeDtos = _mapperEntityToDto.Map<List<MemberTypeDto>>(memberTypes);
            return memberTypeDtos;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RegisterMember(MemberDto memberDto)
        {
            if (memberDto == null)
            {
                var valCheck = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ValidationFailed);
                valCheck.Errors.Add("No data to register member");
                return valCheck;
            }

            if (memberDto.Id == null || memberDto.Id == Guid.Empty)
            {
                memberDto.Id = Guid.NewGuid();
            }

            if (string.IsNullOrWhiteSpace(memberDto.Status))
            {
                memberDto.Status = "New";
            }

            memberDto.IsActive = true;

            var allMemebers = await _memberService.ListMembersBasedOnCustomer(memberDto.CustomerId);
            if(allMemebers != null && allMemebers.Count > 0)
            {
                var isExists = allMemebers.Where(m => m.Email.ToLower() == memberDto.Email.ToLower());
                if (isExists != null && isExists.Count() > 0)
                {
                    var valCheck = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ValidationFailed);
                    valCheck.Errors.Add("Member with same email id already exists...");
                    return valCheck;
                }
            }


            Member member = new Member();
            try
            {
                 member = _mapperDtoToEntity.Map<Member>(memberDto);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                throw;
            }


            member.CreatedDate = DateTime.UtcNow;
            member.LastAction = DateTime.UtcNow;

            Address memberAddress = _mapperDtoToEntity.Map<Address>(memberDto.Address);
            memberAddress.Id = Guid.NewGuid();
            memberAddress.MemberId = memberDto.Id;
            memberAddress.CreatedDate = DateTime.UtcNow;
            memberAddress.LastAction = DateTime.UtcNow;
            member.Address = memberAddress;

            if(memberDto.MemberPhoto != null)
            {
                MemberMedia membermedia = _mapperDtoToEntity.Map<MemberMedia>(memberDto.MemberPhoto);
                membermedia.Id = Guid.NewGuid();
                membermedia.MemberId = memberDto.Id;
                membermedia.CreatedDate = DateTime.UtcNow;
                membermedia.LastAction = DateTime.UtcNow;
                member.MemberPhoto = membermedia;
            }

            //if(memberDto.MemberType_Id != null && memberDto.MemberType_Id != Guid.Empty)
            //{
            //    MemberType memberType = new MemberType();
            //    memberType.Id = memberDto.MemberType_Id;
            //    memberType.CreatedDate = DateTime.UtcNow;
            //    memberType.LastAction = DateTime.UtcNow;
            //    member.MemberType = memberType;
            //}
            
            

            var result = await _memberService.RegisterMember(member);
            return result;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateMemberStatus(MemberDto memberDto)
        {
            Member member = _mapperDtoToEntity.Map<Member>(memberDto);
            var result = await _memberService.UpdateMemberStatus(member);
            return result;
        }
    }
}