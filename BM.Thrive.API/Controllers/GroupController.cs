﻿using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace BM.Thrive.API.Controllers
{
    public class GroupController : ApiController
    {
        IGroupControllerHelper _groupControllerHelper;
        public GroupController(IGroupControllerHelper groupControllerHelper)
        {
            _groupControllerHelper = groupControllerHelper;
        }



        [HttpPost]
        [Route("api/AddGroup")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddGroup(GroupDto groupDto)
        {
            return await _groupControllerHelper.AddGroup(groupDto);
        }

        [HttpPost]
        [Route("api/AddMemberGroupMap")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddMemberGroupMap(MemberGroupMapDto memberGroupMapDto)
        {
            return await _groupControllerHelper.AddMemberGroupMap(memberGroupMapDto);
        }

        [HttpDelete]
        [Route("api/DeleteGroup")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteGroup(Guid groupId)
        {
            return await _groupControllerHelper.DeleteGroup(groupId);
        }

        [HttpDelete]
        [Route("api/DeleteMemberGroupMap")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteMemberGroupMap(Guid memberGroupMapId)
        {
            var result = await _groupControllerHelper.DeleteMemberGroupMap(memberGroupMapId);
            return result;
        }

        [HttpGet]
        [Route("api/GetGroupTypes")]
        public async Task<List<GroupTypeDto>> GetGroupTypes()
        {
            var groupTypeDtos = await _groupControllerHelper.GetGroupTypes();
            return groupTypeDtos;
        }

        [HttpGet]
        [Route("api/ListAllGroups")]
        public async Task<List<GroupDto>> ListAllGroups()
        {
            var groupDtos = await _groupControllerHelper.ListAllGroups();
            return groupDtos;
        }

        [HttpGet]
        [Route("api/ListGroups")]
        public async Task<List<GroupDto>> ListGroups(Guid customerId)
        {
            var groupDtos = await _groupControllerHelper.ListGroups(customerId);
            return groupDtos;
        }
    }
}