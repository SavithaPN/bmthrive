﻿using BM.Thrive.API.Helpers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace BM.Thrive.API.Controllers
{
    public class ApiAuthController : ApiController
    {
        private IApiAuthControllerHelper _authHelper;
        public ApiAuthController(IApiAuthControllerHelper authHelper)
        {
            _authHelper = authHelper;
        }
    }
}