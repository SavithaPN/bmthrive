﻿using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BM.Thrive.API.Controllers
{
    [Authorize]
    public class OfferingController : ApiController
    {
        IOfferingControllerHelper _offeringControllerHelper;
        public OfferingController(IOfferingControllerHelper offeringControllerHelper)
        {
            _offeringControllerHelper = offeringControllerHelper;
        }

        [HttpPost]
        [Route("api/AddOffer")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddOffer(OfferingDto offerDto)
        {
            return await _offeringControllerHelper.AddOffer(offerDto);
        }

        [HttpDelete]
        [Route("api/DeleteOffer")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteOffer(Guid id)
        {
            return await _offeringControllerHelper.DeleteOffer(id);
        }

        [HttpGet]
        [Route("api/SearchOfferings")]
        public async Task<List<OfferingDto>> SearchOfferings(string searchText = "", bool includeExpiredOffer = false)
        {
            return await _offeringControllerHelper.SearchOfferings(searchText, includeExpiredOffer);
        }

        [HttpPut]
        [Route("api/UpdateOffer")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateOffer(OfferingDto offerDto)
        {
            return await _offeringControllerHelper.UpdateOffer(offerDto);
        }
    }
}
