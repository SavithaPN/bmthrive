﻿using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;

namespace BM.Thrive.API.Controllers
{
    [Authorize]
    public class MemberController : ApiController
    {
        IMemberControllerHelper _memberControllerHelper;

        public MemberController(IMemberControllerHelper memberControllerHelper)
        {
            _memberControllerHelper = memberControllerHelper;
        }

        [HttpGet]
        [Route("api/Membership/ListMembersBasedOnCustomer")]
        public async Task<List<MemberDto>> ListMembersBasedOnCustomer(Guid customerId)
        {
            var members = await _memberControllerHelper.ListMembersBasedOnCustomer(customerId);
            return members;
        }

        [HttpGet]
        [Route("api/Membership/ListAllMembers")]
        public async Task<List<MemberDto>> ListAllMembers()
        {
            var members = await _memberControllerHelper.ListAllMembers();
            return members;
        }

        [HttpDelete]
        [Route("api/Membership/DeleteMember")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteMember(Guid memberId)
        {
            return await _memberControllerHelper.DeleteMember(memberId);
        }

        [HttpGet]
        [Route("api/Membership/ListMemberTypes")]
        public async Task<List<MemberTypeDto>> ListMemberTypes()
        {
            var memberTypes = await _memberControllerHelper.ListMemberTypes();
            return memberTypes;
        }

        [HttpGet]
        [Route("api/Membership/ListMembersSorted")]
        public async Task<PagedListViewModel<MemberDto>> ListMembersSorted(Guid customerId, string sortBy="", string search="", int count = -1, int skip = -1)
        {
            var members = await _memberControllerHelper.ListMembersSorted(customerId, sortBy, search, count, skip);
            return members;
        }

        [HttpGet]
        [Route("api/Membership/GetMemberDetails")]
        public async Task<MemberDto> GetMemberDetails(Guid id)
        {
            var members = await _memberControllerHelper.GetMemberDetails(id);
            return members;
        }

        [HttpPost]
        [Route("api/Membership/RegisterMember")]
        public async  Task<WorkResultDto<Guid, GeneralWorkStatus>> RegisterMember(MemberDto memberDto)
        {
            var result = await _memberControllerHelper.RegisterMember(memberDto);
            return result;
        }

        [HttpPost]
        [Route("api/Membership/UpdateMember")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateMember(MemberDto memberDto)
        {
            var result = await _memberControllerHelper.UpdateMemberStatus(memberDto);
            return result;
        }

        [HttpGet]
        [Route("api/Membership/DownloadProfilePic")]
        public async Task<IHttpActionResult> GetMemberMediaFile(Guid memberId)
        {
            var result = await _memberControllerHelper.GetMemberMedia(memberId);
            MemoryStream mStream = null;
            if (result != null && result.MediaFile.Length > 0)
            {
                mStream = new MemoryStream(result.MediaFile.ToArray());
            }

            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.NoContent);
            if (mStream == null)
            {
                return new ResponseMessageResult(httpResponseMessage);
            }

            httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            httpResponseMessage.Content = new StreamContent(mStream);

            httpResponseMessage.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = memberId.ToString() + "." + result.MediaContentType.Substring(result.MediaContentType.IndexOf('/') + 1)
            };

            httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(result.MediaContentType);
            ResponseMessageResult responseMessageResult = ResponseMessage(httpResponseMessage);
            return responseMessageResult;
        }
             
    }
}