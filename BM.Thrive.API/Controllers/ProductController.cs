﻿using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BM.Thrive.API.Controllers
{
    [Authorize]
    public class ProductController : ApiController
    {
        IProductControllerHelper _productControllerHelper;

        public ProductController(IProductControllerHelper productControllerHelper)
        {
            _productControllerHelper = productControllerHelper;
        }

        [HttpPost]
        [Route("api/AddProductPicture")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddProductPicture(ProductImagesDto productImages)
        {
            return await _productControllerHelper.AddProductPicture(productImages);
        }

        [HttpPost]
        [Route("api/AddProductProperties")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddProductProperties(ProductPropertiesDto productProperties)
        {
            return await _productControllerHelper.AddProductProperties(productProperties);
        }

        [HttpPost]
        [Route("api/CreateProductFull")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateProductFull(ProductDto product)
        {
            return await _productControllerHelper.CreateProductFull(product);
        }

        [HttpPost]
        [Route("api/CreateProductLite")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateProductLite(ProductDto product)
        {
            return await _productControllerHelper.CreateProductLite(product);
        }

        [HttpGet]
        [Route("api/GetProductPictures")]
        public async Task<List<ProductImagesDto>> GetProductPicture(Guid productId)
        {
            return await _productControllerHelper.GetProductPicture(productId);
        }

        [HttpDelete]
        [Route("api/RemovePicture")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemovePicture(Guid imageId)
        {
            return await _productControllerHelper.RemovePicture(imageId);
        }

        [HttpDelete]
        [Route("api/RemoveProductPictures")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProductPictures(Guid productId)
        {
            return await _productControllerHelper.RemoveProductPictures(productId);
        }

        [HttpDelete]
        [Route("api/RemoveProperty")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProperty(Guid propertyId)
        {
            return await _productControllerHelper.RemoveProperty(propertyId);
        }

        [HttpGet]
        [Route("api/SearchProduct")]
        public async Task<List<ProductDto>> SearchProduct(string id = "", string name = "", string anyPropertyName = "", string anyPropertyValue = "")
        {
            return await _productControllerHelper.SearchProduct(id, name, anyPropertyName, anyPropertyValue);
        }

        [HttpPut]
        [Route("api/UpdateProduct")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateProduct(ProductDto product)
        {
            return await _productControllerHelper.UpdateProduct(product);
        }

        [HttpPut]
        [Route("api/UpdateProductProperty")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateProductProperty(ProductPropertiesDto productProperties)
        {
            return await _productControllerHelper.UpdateProductProperty(productProperties);
        }
    }
}
