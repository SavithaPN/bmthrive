﻿using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
//using System.Web.Mvc;

namespace BM.Thrive.API.Controllers
{
    [Authorize]
    public class CustomerController : ApiController
    {
        ICustomerControllerHelper _customerHelper;

        public CustomerController(ICustomerControllerHelper customerHelper)
        {
            _customerHelper = customerHelper;
        }

        [HttpGet]
        [Route("api/ListCustomers")]
        public async Task<IEnumerable<CustomerDto>> ListCustomers()
        {
            return await _customerHelper.ListCustomers();
        }

        [HttpPost]
        [Route("api/AddCustomer")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddCustomer(CustomerDto customerDto)
        {
            return await _customerHelper.AddCustomer(customerDto);
        }
    }

}