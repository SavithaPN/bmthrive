﻿using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BM.Thrive.API.Controllers
{
    [Authorize]
    public class ComponentController : ApiController
    {
        IComponentControllerHelper _componentControllerHelper;
        public ComponentController(IComponentControllerHelper componentControllerHelper)
        {
            _componentControllerHelper = componentControllerHelper;
        }

        [HttpPost]
        [Route("api/AddComponentPicture")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddComponentPicture(ComponentImagesDto componentImage)
        {
            return await _componentControllerHelper.AddComponentPicture(componentImage);
        }

        [HttpPost]
        [Route("api/AddComponentProperty")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddComponentProperty(ComponentPropertiesDto componentProperty)
        {
            var result = await _componentControllerHelper.AddComponentProperty(componentProperty);
            return result;
        }

        [HttpPost]
        [Route("api/CreateComponentFull")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateComponentFull(ComponentDto component)
        {
            var result = await _componentControllerHelper.CreateComponentFull(component);
            return result;
        }

        [HttpPost]
        [Route("api/CreateComponentLite")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateComponentLite(ComponentDto component)
        {
            var result = await _componentControllerHelper.CreateComponentLite(component);
            return result;
        }

        [HttpGet]
        [Route("api/GetComponent")]
        public async Task<ComponentDto> GetComponent(Guid id)
        {
            var result = await _componentControllerHelper.GetComponent(id);
            return result;
        }

        [HttpGet]
        [Route("api/GetComponentImages")]
        public async Task<List<ComponentImagesDto>> GetComponentImages(Guid id)
        {
            var result = await _componentControllerHelper.GetComponentImages(id);
            return result;
        }

        [HttpDelete]
        [Route("api/RemovePicture")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemovePicture(Guid imageId)
        {
            var result = await _componentControllerHelper.RemovePicture(imageId);
            return result;
        }

        [HttpDelete]
        [Route("api/RemoveProperty")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProperty(Guid propertyId)
        {
            var result = await _componentControllerHelper.RemoveProperty(propertyId);
            return result;
        }

        [HttpGet]
        [Route("api/SearchComponent")]
        public async Task<List<ComponentDto>> SearchComponent(string searchText = "")
        {
            var result = await _componentControllerHelper.SearchComponent(searchText);
            return result;
        }
    }
}
