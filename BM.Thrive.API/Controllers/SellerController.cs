﻿using BM.Thrive.API.Helpers.Interfaces;
using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BM.Thrive.API.Controllers
{
    [Authorize]
    public class SellerController : ApiController
    {
        ISellerControllerHelper _sellerControllerHelper;
        public SellerController(ISellerControllerHelper sellerControllerHelper)
        {
            _sellerControllerHelper = sellerControllerHelper;
        }

        [HttpPost]
        [Route("api/CreateSeller")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateSeller(SellerDto seller)
        {
            return await _sellerControllerHelper.CreateSeller(seller);
        }

        [HttpDelete]
        [Route("api/DeleteSeller")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteSeller(Guid id)
        {
            return await _sellerControllerHelper.DeleteSeller(id);
        }

        [HttpGet]
        [Route("api/GetSeller")]
        public async Task<SellerDto> GetSeller(Guid id)
        {
            return await _sellerControllerHelper.GetSeller(id);
        }

        [HttpGet]
        [Route("api/SearchSeller")]
        public async Task<List<SellerDto>> SearchSeller(string address = "", string name = "")
        {
            return await _sellerControllerHelper.SearchSeller(address, name);
        }

        [HttpPut]
        [Route("api/UpdateSeller")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateSeller(SellerDto seller)
        {
            return await _sellerControllerHelper.UpdateSeller(seller);
        }
    }
}
