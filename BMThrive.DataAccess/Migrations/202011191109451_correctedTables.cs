﻿namespace BMThrive.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class correctedTables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Members", "MembershipExpiry", c => c.DateTime());
            AddColumn("dbo.Products", "SellerId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Products", "SellerId");
            CreateIndex("dbo.Offerings", "ProductId");
            CreateIndex("dbo.Offerings", "SellerId");
            AddForeignKey("dbo.Products", "SellerId", "dbo.Sellers", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Offerings", "ProductId", "dbo.Products", "Id", cascadeDelete: false);
            AddForeignKey("dbo.Offerings", "SellerId", "dbo.Sellers", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Offerings", "SellerId", "dbo.Sellers");
            DropForeignKey("dbo.Offerings", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Products", "SellerId", "dbo.Sellers");
            DropIndex("dbo.Offerings", new[] { "SellerId" });
            DropIndex("dbo.Offerings", new[] { "ProductId" });
            DropIndex("dbo.Products", new[] { "SellerId" });
            DropColumn("dbo.Products", "SellerId");
            DropColumn("dbo.Members", "MembershipExpiry");
        }
    }
}
