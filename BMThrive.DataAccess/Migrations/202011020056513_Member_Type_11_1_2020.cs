﻿namespace BMThrive.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Member_Type_11_1_2020 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Members", "MemberType_Id", c => c.Guid());
            CreateIndex("dbo.Members", "MemberType_Id");
            AddForeignKey("dbo.Members", "MemberType_Id", "dbo.MemberTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Members", "MemberType_Id", "dbo.MemberTypes");
            DropIndex("dbo.Members", new[] { "MemberType_Id" });
            DropColumn("dbo.Members", "MemberType_Id");
        }
    }
}
