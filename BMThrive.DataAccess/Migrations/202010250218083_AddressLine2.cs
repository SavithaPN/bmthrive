﻿namespace BMThrive.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddressLine2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Addresses", "Address2", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Addresses", "Address2");
        }
    }
}
