﻿namespace BMThrive.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Member_Type_11_1_2020_2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Members", "Type_Id", "dbo.MemberTypes");
            DropIndex("dbo.Members", new[] { "Type_Id" });
            DropColumn("dbo.Members", "Type_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Members", "Type_Id", c => c.Guid());
            CreateIndex("dbo.Members", "Type_Id");
            AddForeignKey("dbo.Members", "Type_Id", "dbo.MemberTypes", "Id");
        }
    }
}
