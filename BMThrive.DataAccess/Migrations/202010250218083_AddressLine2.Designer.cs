﻿// <auto-generated />
namespace BMThrive.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class AddressLine2 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddressLine2));
        
        string IMigrationMetadata.Id
        {
            get { return "202010250218083_AddressLine2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
