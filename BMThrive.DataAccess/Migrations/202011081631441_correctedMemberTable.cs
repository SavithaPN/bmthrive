﻿namespace BMThrive.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class correctedMemberTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Members", "MemberType_Id", "dbo.MemberTypes");
            DropIndex("dbo.Members", new[] { "MemberType_Id" });
            AlterColumn("dbo.Members", "MemberType_Id", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Members", "MemberType_Id", c => c.Guid());
            CreateIndex("dbo.Members", "MemberType_Id");
            AddForeignKey("dbo.Members", "MemberType_Id", "dbo.MemberTypes", "Id");
        }
    }
}
