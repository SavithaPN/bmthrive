﻿namespace BMThrive.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedGroupTypeCol : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Groups", "GroupTypeId", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Groups", "GroupTypeId");
        }
    }
}
