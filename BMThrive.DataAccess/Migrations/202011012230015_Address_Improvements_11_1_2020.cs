﻿namespace BMThrive.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Address_Improvements_11_1_2020 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Addresses", "District", c => c.String());
            AddColumn("dbo.Addresses", "WardNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Addresses", "WardNumber");
            DropColumn("dbo.Addresses", "District");
        }
    }
}
