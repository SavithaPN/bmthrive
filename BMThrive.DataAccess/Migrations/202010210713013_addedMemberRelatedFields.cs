﻿namespace BMThrive.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedMemberRelatedFields : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MemberMedias",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        MemberId = c.Guid(nullable: false),
                        MediaFile = c.Binary(),
                        MediaContentType = c.String(),
                        Active = c.Boolean(nullable: false),
                        MediaFileDownloadUrl = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Members", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.MemberTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Members", "IsActive", c => c.Boolean(nullable: false));
            AddColumn("dbo.Members", "Type_Id", c => c.Guid());
            CreateIndex("dbo.Members", "Type_Id");
            AddForeignKey("dbo.Members", "Type_Id", "dbo.MemberTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Members", "Type_Id", "dbo.MemberTypes");
            DropForeignKey("dbo.MemberMedias", "Id", "dbo.Members");
            DropIndex("dbo.MemberMedias", new[] { "Id" });
            DropIndex("dbo.Members", new[] { "Type_Id" });
            DropColumn("dbo.Members", "Type_Id");
            DropColumn("dbo.Members", "IsActive");
            DropTable("dbo.MemberTypes");
            DropTable("dbo.MemberMedias");
        }
    }
}
