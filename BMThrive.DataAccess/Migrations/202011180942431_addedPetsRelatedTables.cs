﻿namespace BMThrive.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedPetsRelatedTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sellers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        SellerIdNumber1 = c.String(),
                        SellerIdNumber2 = c.String(),
                        LicenseNumber = c.String(),
                        AdditionalInformation = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.ContactInformations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SellerId = c.Guid(nullable: false),
                        HomePhoneNumber = c.String(),
                        MobileNumber = c.String(),
                        EmailId = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sellers", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.CategoryTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ComponentImages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ComponentId = c.Guid(nullable: false),
                        Type = c.String(),
                        Image = c.Binary(),
                        SlNo = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Components", t => t.ComponentId, cascadeDelete: true)
                .Index(t => t.ComponentId);
            
            CreateTable(
                "dbo.Components",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ComponentProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ComponentId = c.Guid(nullable: false),
                        Name = c.String(),
                        ComponentPropertyType = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Components", t => t.ComponentId, cascadeDelete: true)
                .Index(t => t.ComponentId);
            
            CreateTable(
                "dbo.Offerings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        SellerId = c.Guid(nullable: false),
                        OfferDescription = c.String(),
                        NoOfItemsAvailable = c.Int(nullable: false),
                        PackageFormat = c.String(),
                        ExpiryDate = c.DateTime(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductImages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        Type = c.String(),
                        Image = c.Binary(),
                        SlNo = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Unit = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                        Category_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryTypes", t => t.Category_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.ProductProperties",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                        Name = c.String(),
                        ProductPropertyType = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            AddColumn("dbo.Addresses", "SellerId", c => c.Guid());
            AlterColumn("dbo.Addresses", "MemberId", c => c.Guid());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductProperties", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductImages", "ProductId", "dbo.Products");
            DropForeignKey("dbo.Products", "Category_Id", "dbo.CategoryTypes");
            DropForeignKey("dbo.ComponentProperties", "ComponentId", "dbo.Components");
            DropForeignKey("dbo.ComponentImages", "ComponentId", "dbo.Components");
            DropForeignKey("dbo.ContactInformations", "Id", "dbo.Sellers");
            DropForeignKey("dbo.Sellers", "Id", "dbo.Addresses");
            DropIndex("dbo.ProductProperties", new[] { "ProductId" });
            DropIndex("dbo.Products", new[] { "Category_Id" });
            DropIndex("dbo.ProductImages", new[] { "ProductId" });
            DropIndex("dbo.ComponentProperties", new[] { "ComponentId" });
            DropIndex("dbo.ComponentImages", new[] { "ComponentId" });
            DropIndex("dbo.ContactInformations", new[] { "Id" });
            DropIndex("dbo.Sellers", new[] { "Id" });
            AlterColumn("dbo.Addresses", "MemberId", c => c.Guid(nullable: false));
            DropColumn("dbo.Addresses", "SellerId");
            DropTable("dbo.ProductProperties");
            DropTable("dbo.Products");
            DropTable("dbo.ProductImages");
            DropTable("dbo.Offerings");
            DropTable("dbo.ComponentProperties");
            DropTable("dbo.Components");
            DropTable("dbo.ComponentImages");
            DropTable("dbo.CategoryTypes");
            DropTable("dbo.ContactInformations");
            DropTable("dbo.Sellers");
        }
    }
}
