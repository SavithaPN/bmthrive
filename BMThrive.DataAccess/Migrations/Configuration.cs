﻿namespace BMThrive.DataAccess.Migrations
{
    using BMThrive.Common.Dto.Constants;
    using BMThrive.DataAccess.Contexts;
    using BMThrive.Domain.Entities;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<BMThriveDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
        private string DefaultDateFormat = "MM-dd-yyyy HH:mm:ss";
        private string IndiaDateFormat = "dd-MM-yyyy HH:mm:ss";
        private string DefaultCountry = "USA";
        private Guid CreatedBy;
        private Guid LastActionBy;
        private Guid DefaultCustomerId = new Guid("193da4f9-e3cd-4ae0-9b1b-28232c163b62");
        private Guid RDPCustomerId = new Guid("9fe76248-9a01-459e-b065-eb0c3a1bdb2b");
        

        protected override void Seed(BMThriveDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            CreatedBy = Guid.NewGuid();
            LastActionBy = Guid.NewGuid();
            CreateRolesAndUsers(new ApplicationIdentityDbContext(), context);
            AddDefaultGroupTypes(context);
            AddDefaultMemberTypes(context);
            AddDefaultCustomer(context);
        }

        private void CreateRolesAndUsers(ApplicationIdentityDbContext appContext, BMThriveDBContext soContext)
        {
            var roleManager = new RoleManager<ApplicationUserRole>(new RoleStore<ApplicationUserRole>(appContext));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(appContext));

            if (!roleManager.RoleExists("Admin"))
            {
                //var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                var role = new ApplicationUserRole();
                role.Id = Guid.NewGuid().ToString();
                role.Name = "Admin";
                role.CreatedDate = DateTime.UtcNow;
                role.LastAction = DateTime.UtcNow;
                role.CreatedBy = CreatedBy;
                role.LastActionBy = LastActionBy;
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "admin";
                user.Email = "admin@brainmeshai.com";
                user.EmailConfirmed = true;
                user.CreatedDate = DateTime.UtcNow;
                user.LastAction = DateTime.UtcNow;
                user.RoleId = Guid.Parse(role.Id);
                user.Active = true;
                string adminPWD = "admin123";
                var adminUser = userManager.Create(user, adminPWD);

                if (adminUser.Succeeded)
                {
                    var result = userManager.AddToRole(user.Id, "Admin");
                }
            }
        }

        private void AddDefaultCustomer(BMThriveDBContext context)
        {
            Customer oCustomer = new Customer
            {
                Id = DefaultCustomerId
            };

            if (context.Customers.Where(p => p.Id == oCustomer.Id).Count() <= 0)
            {
                oCustomer.CustomerName = "Default";
                oCustomer.Group = "Default";
                oCustomer.Active = true;
                oCustomer.Country = DefaultCountry;
                oCustomer.DateTimeFormat = DefaultDateFormat;
                oCustomer.Address = "US KY";
                oCustomer.State = "Kentucky";
                oCustomer.CreatedDate = DateTime.UtcNow;
                oCustomer.CreatedBy = CreatedBy;
                oCustomer.LastAction = DateTime.UtcNow;
                oCustomer.LastActionBy = LastActionBy;
                context.Customers.Add(oCustomer);
                context.SaveChanges();
            }

            oCustomer = new Customer
            {
                Id = RDPCustomerId
            };
            if (context.Customers.Where(p => p.Id == oCustomer.Id).Count() <= 0)
            { 
                oCustomer.CustomerName = "Karanataka Ranadheera Pade";
                oCustomer.Group = "KRP";
                oCustomer.Active = true;
                oCustomer.Country = "India";
                oCustomer.DateTimeFormat = IndiaDateFormat;
                oCustomer.Address = "India KA";
                oCustomer.State = "Karanataka";
                oCustomer.CreatedDate = DateTime.UtcNow;
                oCustomer.CreatedBy = CreatedBy;
                oCustomer.LastAction = DateTime.UtcNow;
                oCustomer.LastActionBy = LastActionBy;
                context.Customers.Add(oCustomer);
                context.SaveChanges();
            }
        }

        private void AddDefaultMemberTypes(BMThriveDBContext context)
        {
            if(context.MemberTypes.Count() <= 0)
            {
                MemberType type = new MemberType
                {
                    Id = BMThriveConstants.MemberConstants.DefaultGoldTypeId,
                    Name = "Gold",
                    CreatedDate = DateTime.UtcNow,
                    LastAction = DateTime.UtcNow,
                    LastActionBy = LastActionBy,
                    CreatedBy = CreatedBy
                };
                context.MemberTypes.Add(type);
                type = new MemberType
                {
                    Id = BMThriveConstants.MemberConstants.DefaultSilverTypeId,
                    Name = "Silver",
                    CreatedDate = DateTime.UtcNow,
                    LastAction = DateTime.UtcNow,
                    LastActionBy = LastActionBy,
                    CreatedBy = CreatedBy
                };
                context.MemberTypes.Add(type);
                type = new MemberType
                {
                    Id = BMThriveConstants.MemberConstants.DefaultBronzeTypeId,
                    Name = "Bronze",
                    CreatedDate = DateTime.UtcNow,
                    LastAction = DateTime.UtcNow,
                    LastActionBy = LastActionBy,
                    CreatedBy = CreatedBy
                };

                context.MemberTypes.Add(type);
                context.SaveChanges();
            }
        }

        private void AddDefaultGroupTypes(BMThriveDBContext context)
        {
            if(context.GroupTypes.Count() <= 0)
            {
                GroupType type = new GroupType
                {
                    Id = BMThriveConstants.GroupConstants.DefaultRegionalGroupTypeId,
                    Name = "Regional",
                    CreatedDate = DateTime.UtcNow,
                    LastAction = DateTime.UtcNow,
                    LastActionBy = LastActionBy,
                    CreatedBy = CreatedBy
                };
                context.GroupTypes.Add(type);

                type = new GroupType
                {
                    Id = BMThriveConstants.GroupConstants.DefaultServicesGroupTypeId,
                    Name = "Services",
                    CreatedDate = DateTime.UtcNow,
                    LastAction = DateTime.UtcNow,
                    LastActionBy = LastActionBy,
                    CreatedBy = CreatedBy
                };

                context.GroupTypes.Add(type);
                context.SaveChanges();
            }
        }
    }
}
