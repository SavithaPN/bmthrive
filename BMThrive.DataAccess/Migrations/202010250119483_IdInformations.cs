﻿namespace BMThrive.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdInformations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Members", "IdInfoPrimary", c => c.String());
            AddColumn("dbo.Members", "IdInfoSecondry", c => c.String());
            AddColumn("dbo.Members", "IdInfoAddtional", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Members", "IdInfoAddtional");
            DropColumn("dbo.Members", "IdInfoSecondry");
            DropColumn("dbo.Members", "IdInfoPrimary");
        }
    }
}
