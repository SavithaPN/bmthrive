﻿// <auto-generated />
namespace BMThrive.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class AddedGroupRelatedTables : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedGroupRelatedTables));
        
        string IMigrationMetadata.Id
        {
            get { return "202010060631568_AddedGroupRelatedTables"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
