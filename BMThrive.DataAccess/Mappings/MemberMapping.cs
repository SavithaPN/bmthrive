﻿using BMThrive.Domain;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.DataAccess.Mappings
{
    public class MemberMapping : EntityTypeConfiguration<Member>
    {
        public MemberMapping()
        {
            //Table Name
            this.ToTable("Members");

            //Primary Key
            this.HasKey(k => k.Id);
            this.Property(m => m.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.HasRequired(m => m.Address).WithRequiredPrincipal(ad => ad.Member);
            this.HasRequired(m => m.MemberPhoto).WithRequiredPrincipal(md => md.Member);
        }
    }

    public class AddressMapping : EntityTypeConfiguration<Address>
    {
        public AddressMapping()
        {
            //table 
            ToTable("Addresses");

            // Primary Key
            this.HasKey(e => e.Id);
            this.Property(e => e.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

           
        }
    }

    public class MemberMediaMapping : EntityTypeConfiguration<MemberMedia>
    {
        public MemberMediaMapping()
        {
            ToTable("MemberMedias");

            // Primary Key
            this.HasKey(e => e.Id);
            this.Property(e => e.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }
}
