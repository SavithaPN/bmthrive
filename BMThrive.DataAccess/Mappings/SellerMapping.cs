﻿using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.DataAccess.Mappings
{
    public class SellerMapping : EntityTypeConfiguration<Seller>
    {
        public SellerMapping()
        {
            //Table Name
            this.ToTable("Sellers");

            //Primary key
            this.HasKey(s => s.Id);
            this.Property(s => s.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.HasRequired(a => a.Address).WithRequiredPrincipal(s => s.Seller);
            this.HasRequired(c => c.ContactInfo).WithRequiredPrincipal(s => s.Seller);
            
            
        }
    }

    public class ContactInformationMapping : EntityTypeConfiguration<ContactInformation>
    {
        public ContactInformationMapping()
        {
            //Table Name
            this.ToTable("ContactInformations");

            //Primary Key
            this.HasKey(c => c.Id);
            this.Property(c => c.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
        }
    }

    public class ProductPropertiesMapping : EntityTypeConfiguration<ProductProperties>
    {
        public ProductPropertiesMapping()
        {
            //Table Name
            this.ToTable("ProductProperties");

            //Primary Key
            this.HasKey(pp => pp.Id);
            this.Property(pp => pp.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.HasRequired(pp => pp.Product).WithMany(p => p.ProductProperties).HasForeignKey(pp => pp.ProductId);
        }
    }
    public class ProductImagesMapping : EntityTypeConfiguration<ProductImages>
    {
        public ProductImagesMapping()
        {
            //Table Name
            this.ToTable("ProductImages");

            //Primary Key
            this.HasKey(pi => pi.Id);
            this.Property(pi => pi.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.HasRequired(pi => pi.Product).WithMany(i => i.ProductImages).HasForeignKey(p => p.ProductId);
        }
    }


    public class ComponentImagesMapping : EntityTypeConfiguration<ComponentImages>
    {
        public ComponentImagesMapping()
        {
            // Table name
            this.ToTable("ComponentImages");

            //Primary Key
            this.HasKey(ci => ci.Id);
            this.Property(ci => ci.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            //Foreign Key configuration
            this.HasRequired(ci => ci.Component).WithMany(i => i.ComponentImages).HasForeignKey(c => c.ComponentId);
        }
    }

    public class ComponentPropertiesMapping : EntityTypeConfiguration<ComponentProperties>
    {
        public ComponentPropertiesMapping()
        {
            //Table Name
            this.ToTable("ComponentProperties");

            //Primary Key
            this.HasKey(cp => cp.Id);
            this.Property(cp => cp.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            //Foreign Key configuration
            this.HasRequired(cp => cp.Component).WithMany(p => p.ComponentProperties).HasForeignKey(c => c.ComponentId);
        }
    }

    public class OfferingMapping : EntityTypeConfiguration<Offering>
    {
        public OfferingMapping()
        {
            //Table Name
            this.ToTable("Offerings");

            //Primary key
            this.HasKey(s => s.Id);
            this.Property(s => s.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            //Foreign key configuration
            //this.HasRequired(o => o.Seller).WithMany(s => s.Offerings).HasForeignKey(s => s.SellerId).WillCascadeOnDelete(false);
            //this.HasRequired(pp => pp.Product).WithMany(p => p.ProductProperties).HasForeignKey(pp => pp.ProductId);

        }
    }

    public class ProductMapping : EntityTypeConfiguration<Product>
    {
        public ProductMapping()
        {
            //Table Name
            this.ToTable("Products");

            //Primary Key
            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasColumnName("Id").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.HasRequired(o => o.Seller).WithMany(s => s.Products).HasForeignKey(s => s.SellerId).WillCascadeOnDelete(false);
        }
    }

}
