﻿using BMThrive.DataAccess.Contexts;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.DataAccess.Initializer
{
    public class MigrationContextFactory : IDbContextFactory<BMThriveDBContext>
    {
        public BMThriveDBContext Create()
        {
            return new BMThriveDBContext("BMThriveDBContext");
        }
    }
}
