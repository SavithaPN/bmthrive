﻿using BMThrive.DataAccess.Contexts;
using BMThrive.DataAccess.Migrations;
using System.Data.Entity;

namespace BMThrive.DataAccess.Initializer
{
    public  class CustomInitializer : MigrateDatabaseToLatestVersion<BMThriveDBContext, Configuration>
    {
        public override void InitializeDatabase(BMThriveDBContext context)
        {
            base.InitializeDatabase(context);
        }

    }
}
