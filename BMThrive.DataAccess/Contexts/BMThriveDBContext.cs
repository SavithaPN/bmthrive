﻿using BMThrive.DataAccess.Migrations;
using BMThrive.Domain;
using BMThrive.Domain.Entities;
using System.Data.Entity;

namespace BMThrive.DataAccess.Contexts
{
    public class BMThriveDBContext : DbContext
    {
        public BMThriveDBContext(string conn): base(conn)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<BMThriveDBContext, Configuration>());

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
        }

        public DbSet<Customer>  Customers { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupType> GroupTypes { get; set; }
        public DbSet<MemberGroupMap> MemberGroupMaps { get; set; }
        public DbSet<MemberType> MemberTypes { get; set; }
        public DbSet<MemberMedia> MemberMedias { get; set; }
        public DbSet<CategoryType> CategoryTypes { get; set; }
        public DbSet<ContactInformation> ContactInformations { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductImages> ProductImages { get; set; }
        public DbSet<Seller> Sellers { get; set; }
        public DbSet<Offering> Offerings { get; set; }
        public DbSet<ProductProperties> ProductProperties { get; set; }
        public DbSet<Component> Components { get; set; }
        public DbSet<ComponentImages> ComponentImages { get; set; }
        public DbSet<ComponentProperties> ComponentProperties { get; set; }
    }
}
