﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Interfaces
{
    public interface IMemberService
    {
        Task<WorkResultDto<Guid, GeneralWorkStatus>> RegisterMember(Member member);
        Task<List<Member>> ListMembersBasedOnCustomer(Guid customerId);
        Task<List<Member>> ListAllMembers();
        Task<PagedListViewModel<Member>> ListMembersSorted(Guid customerId, string sortBy = "", string search = "", int count = -1, int skip = -1);
        Task<Member> GetMemberDetails(Guid id);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateMemberStatus(Member memberDto);
        Task<List<MemberType>> ListMemberTypes();
        Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteMember(Guid memberId);

        Task<List<Member>> ListAllMemberWithOutMedia();
        Task<List<Member>> ListMembersBasedOnCustomerWithoutMedia(Guid customerId);
        Task<PagedListViewModel<Member>> ListMembersSortedWithOutMedia(Guid customerId, string sortBy = "", string search = "", int count = -1, int skip = -1);

        Task<MemberMedia> GetMemberMedia(Guid memberId);
    }
}
