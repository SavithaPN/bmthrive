﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Interfaces
{
    public interface ISellerService
    {
        Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateSeller(Seller seller);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateSeller(Seller seller);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteSeller(Guid id);
        Task<Seller> GetSeller(Guid id);
        Task<List<Seller>> SearchSeller(string address = "", string name = "");

    }
}
