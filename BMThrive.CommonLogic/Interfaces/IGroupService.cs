﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Interfaces
{
    public interface IGroupService
    {
        Task<List<Group>> ListGroups(Guid customerId);
        Task<List<Group>> ListAllGroups();
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddGroup(Group group);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteGroup(Guid groupId);

        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddMemberGroupMap(MemberGroupMap memberGroupMap);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteMemberGroupMap(Guid memberGroupMapId);

        Task<List<GroupType>> GetGroupTypes();
    }
}
