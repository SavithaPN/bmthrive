﻿using BMThrive.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Interfaces
{
    public interface IApiAuthService
    {
        AuthResponseDto Authenticate(AuthInformationDto user);
    }
}
