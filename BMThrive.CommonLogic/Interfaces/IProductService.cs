﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Interfaces
{
    public interface IProductService
    {
        Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateProductFull(Product product);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateProductLite(Product product);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddProductProperties(ProductProperties productProperties);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddProductPicture(ProductImages productImages);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProductPictures(Guid productId);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> RemovePicture(Guid imageId);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProperty(Guid propertyId);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateProduct(Product product);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateProductProperty(ProductProperties productProperties);
        Task<List<Product>> SearchProduct(string id="", string name="", string anyPropertyName="",string anyPropertyValue = "");
        Task<List<ProductImages>> GetProductPicture(Guid productId);
    }
}
