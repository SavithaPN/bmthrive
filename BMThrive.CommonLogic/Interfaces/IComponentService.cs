﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Interfaces
{
    public interface IComponentService
    {
        Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateComponentFull(Component component);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateComponentLite(Component component);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddComponentProperty(ComponentProperties componentProperty);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddComponentPicture(ComponentImages componentImage);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> RemovePicture(Guid imageId);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProperty(Guid propertyId);
        Task<Component> GetComponent(Guid id);
        Task<List<Component>> SearchComponent(string searchText = "");
        Task<List<ComponentImages>> GetComponentImages(Guid id);
    }
}
