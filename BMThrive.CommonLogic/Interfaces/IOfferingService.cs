﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Interfaces
{
    public interface IOfferingService
    {
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddOffer(Offering offer);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateOffer(Offering offer);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteOffer(Guid id);
        Task<List<Offering>> SearchOfferings(string searchText = "", bool includeExpiredOffer= false);
    }
}
