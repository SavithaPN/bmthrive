﻿using BMThrive.Common.Dto;
using BMThrive.Domain.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Interfaces
{
    public interface IUserService
    {
        Task<IdentityResult> RegisterUser(ApplicationUser user, string password);
        Task<IdentityResult> UpdateUser(ApplicationUser user);
        Task<IdentityResult> DeleteRole(Guid id);
        Task<ApplicationUser> FindUser(string userName, string password);
        Task<ApplicationUser> FindUserById(string id);
        List<ApplicationUser> GetUsers();
        Task<IdentityResult> AddRole(ApplicationUserRole role);
        Task<IdentityResult> EditRole(ApplicationUserRole role);
        List<ApplicationUserRole> GetRoles();
        List<ApplicationUserRole> GetRolesById(string RoleId);
        List<ApplicationUserRole> GetAllRoles();
        List<ApplicationUserRole> GetRolesBasedOnCustomer(string customerId);
        PagedListViewModel<ApplicationUser> GetUsersPL(string customerId, int iStarting, int iNumber, string searchText = "");
    }
}
