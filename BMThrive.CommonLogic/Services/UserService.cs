﻿using BMThrive.Common.Dto;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.Domain.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Services
{
    public class UserService : IUserService
    {
        public Task<IdentityResult> AddRole(ApplicationUserRole role)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> DeleteRole(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> EditRole(ApplicationUserRole role)
        {
            throw new NotImplementedException();
        }

        public Task<ApplicationUser> FindUser(string userName, string password)
        {
            throw new NotImplementedException();
        }

        public Task<ApplicationUser> FindUserById(string id)
        {
            throw new NotImplementedException();
        }

        public List<ApplicationUserRole> GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public List<ApplicationUserRole> GetRoles()
        {
            throw new NotImplementedException();
        }

        public List<ApplicationUserRole> GetRolesBasedOnCustomer(string customerId)
        {
            throw new NotImplementedException();
        }

        public List<ApplicationUserRole> GetRolesById(string RoleId)
        {
            throw new NotImplementedException();
        }

        public List<ApplicationUser> GetUsers()
        {
            throw new NotImplementedException();
        }

        public PagedListViewModel<ApplicationUser> GetUsersPL(string customerId, int iStarting, int iNumber, string searchText = "")
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> RegisterUser(ApplicationUser user, string password)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> UpdateUser(ApplicationUser user)
        {
            throw new NotImplementedException();
        }
    }
}
