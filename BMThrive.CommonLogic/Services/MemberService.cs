﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.DataAccess.Repository.Interfaces;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Services
{
    public class MemberService : IMemberService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Member> _memberRespository;
        private readonly IRepository<MemberType> _memberTypeRepository;
        private readonly IRepository<MemberMedia> _memberMediaRepository;

        public MemberService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _memberRespository = unitOfWork.GetRepository<Member>();
            _memberTypeRepository = unitOfWork.GetRepository<MemberType>();
            _memberMediaRepository = unitOfWork.GetRepository<MemberMedia>();
        }
        public async Task<Member> GetMemberDetails(Guid id)
        {
            try
            {
                var member = await _memberRespository.GetAsync(m => m.Id == id);
                return member.FirstOrDefault();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                return null;
            }
        }

        public async Task<List<Member>> ListAllMembers()
        {
            return await ListMembers(true);
        }

        public async Task<List<Member>> ListMembersBasedOnCustomer(Guid customerId)
        {
            return await CustomerBasedMembers(customerId, true);
        }

        public async Task<PagedListViewModel<Member>> ListMembersSorted(Guid customerId, string sortBy = "", string search = "", int count = -1, int skip = -1)
        {
            return await ListSortedMembers(customerId, true, sortBy, search, count, skip);
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RegisterMember(Member member)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _memberRespository.Insert(member);
                await _unitOfWork.SaveAsync();
                return new WorkResultDto<Guid, GeneralWorkStatus>(member.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateMemberStatus(Member member)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var fromStore = await _memberRespository.GetAsync(m => m.Id == member.Id);
                var memberFromStore = fromStore.FirstOrDefault();
                memberFromStore.Status = member.Status;
              
                _memberRespository.Update(memberFromStore);
                await _unitOfWork.SaveAsync();
                result = new WorkResultDto<Guid, GeneralWorkStatus>(member.Id, GeneralWorkStatus.Success);
                return result;
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<List<MemberType>> ListMemberTypes()
        {
            try
            {
                var memberTypes = await _memberTypeRepository.GetAsync();
                return memberTypes.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                return null;

            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteMember(Guid memberId)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var fromStore = await _memberRespository.GetAsync(m => m.Id == memberId);
                var memberFromStore = fromStore.FirstOrDefault();
                memberFromStore.IsActive = false;

                _memberRespository.Update(memberFromStore);
                await _unitOfWork.SaveAsync();
                result = new WorkResultDto<Guid, GeneralWorkStatus>(memberId, GeneralWorkStatus.Success);
                return result;
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<List<Member>> ListMembersBasedOnCustomerWithoutMedia(Guid customerId)
        {
            return await CustomerBasedMembers(customerId, false);
        }

        public async Task<List<Member>> ListAllMemberWithOutMedia()
        {
            return await ListMembers(false);
        }

        public async Task<PagedListViewModel<Member>> ListMembersSortedWithOutMedia(Guid customerId, string sortBy = "", string search = "", int count = -1, int skip = -1)
        {
            return await ListSortedMembers(customerId, false, sortBy, search, count, skip);
        }

        private async Task<PagedListViewModel<Member>> ListSortedMembers(Guid customerId, bool withMedia=false, string sortBy = "", string search = "", int count = -1, int skip = -1)
        {
            PagedListViewModel<Member> membersPl = new PagedListViewModel<Member>();
            IEnumerable<Member> members = null;
            if (withMedia)
            {
                //members = await _memberRespository.GetAsync(m => m.CustomerId == customerId && m.IsActive, null, "Address, MemberMedia");
                members = await _memberRespository.GetAsync(m => m.CustomerId == customerId && m.IsActive, null, "Address");
            }
            else
            {
                members = await _memberRespository.GetAsync(m => m.CustomerId == customerId && m.IsActive);
            }
       
            members = members.ToList();
            try
            {
                if (!string.IsNullOrWhiteSpace(search))
                {
                    members = members.ToList().Where(m => m.Name.Contains(search) || m.Address.City.Contains(search) || m.Address.Country.Contains(search) || m.Address.District.Contains(search));
                }


                if (!string.IsNullOrWhiteSpace(sortBy))
                {
                    sortBy = sortBy.ToLower();
                    switch (sortBy)
                    {
                        case "state":
                            members = members.ToList().OrderBy(m => m.Address.State);
                            break;
                        case "city":
                            members = members.ToList().OrderBy(m => m.Address.City);
                            break;
                        case "country":
                            members = members.ToList().OrderBy(m => m.Address.Country);
                            break;
                        default:
                            members = members.ToList().OrderBy(m => m.RegisteredDate);
                            break;

                    }
                }

                membersPl.TotalItemCount = members.Count();

                if (count > 0 && skip > 0)
                {
                    members = members.ToList().Skip(skip).Take(count);
                }

                membersPl.PageItems = members.ToList();

                return membersPl;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                return null;
            }
        }

        private async Task<List<Member>> CustomerBasedMembers(Guid customerId, bool withMedia = false)
        {
            try
            {
                IEnumerable<Member> members = null;
                if (withMedia)
                {
                    members = await _memberRespository.GetAsync(m => m.CustomerId == customerId && m.IsActive, null, "Address, MemberMedia");
                }
                else
                {
                    members = await _memberRespository.GetAsync(m => m.CustomerId == customerId && m.IsActive);
                }
                
                return members.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                return null;
            }
        }

        private async Task<List<Member>> ListMembers(bool withMedia=false)
        {
            try
            {
                IEnumerable<Member> members = null;
                if (withMedia)
                {
                    members = await _memberRespository.GetAsync(m => m.IsActive, null, "Address, MemberMedia");
                }
                else
                {
                    members = await _memberRespository.GetAsync(m => m.IsActive);
                }
             
                return members.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                return null;
            }
        }

        public async Task<MemberMedia> GetMemberMedia(Guid memberId)
        {
            try
            {
                var memberMedia = await _memberMediaRepository.GetAsync(mm => mm.MemberId == memberId);
                return memberMedia.FirstOrDefault();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                return null;
            }
        }
    }
}
