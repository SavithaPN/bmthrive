﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.DataAccess.Repository.Interfaces;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Services
{
    public class ProductService : IProductService
    {
        IUnitOfWork _unitOfWork;
        IRepository<Product> _productRepository;
        IRepository<ProductProperties> _productPropertiesRespository;
        IRepository<ProductImages> _productImagesRepository;
        public ProductService(IUnitOfWork unitOfwork)
        {
            _unitOfWork = unitOfwork;
            _productRepository = _unitOfWork.GetRepository<Product>();
            _productImagesRepository = _unitOfWork.GetRepository<ProductImages>();
            _productPropertiesRespository = _unitOfWork.GetRepository<ProductProperties>();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddProductPicture(ProductImages productImages)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _productImagesRepository.Insert(productImages);
                await _unitOfWork.SaveAsync();
                result = new WorkResultDto<Guid, GeneralWorkStatus>(productImages.Id, GeneralWorkStatus.Success);
                return result;
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddProductProperties(ProductProperties productProperties)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _productPropertiesRespository.Insert(productProperties);
                await _unitOfWork.SaveAsync();
                result = new WorkResultDto<Guid, GeneralWorkStatus>(productProperties.Id, GeneralWorkStatus.Success);
                return result;
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateProductFull(Product product)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _productRepository.Insert(product);
                await _unitOfWork.SaveAsync();
                result = new WorkResultDto<Guid, GeneralWorkStatus>(product.Id, GeneralWorkStatus.Success);
                return result;
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateProductLite(Product product)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _productRepository.Insert(product);
                await _unitOfWork.SaveAsync();
                result = new WorkResultDto<Guid, GeneralWorkStatus>(product.Id, GeneralWorkStatus.Success);
                return result;
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<List<ProductImages>> GetProductPicture(Guid productId)
        {
            var productImages = await _productImagesRepository.GetAsync(p => p.ProductId == productId);
            return productImages.ToList();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemovePicture(Guid imageId)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var fromShelf = _productImagesRepository.GetById(imageId);
                if(fromShelf != null)
                {
                    fromShelf.IsActive = false;
                }
                _productImagesRepository.Update(fromShelf);
                await _unitOfWork.SaveAsync();
                result = new WorkResultDto<Guid, GeneralWorkStatus>(imageId, GeneralWorkStatus.Success);
                return result;

            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProductPictures(Guid productId)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var imagesfromShelf = await _productImagesRepository.GetAsync(pi => pi.ProductId == productId);
                if(imagesfromShelf != null && imagesfromShelf.Count() > 0)
                {
                    foreach(var image in imagesfromShelf)
                    {
                        var fromShelf = _productImagesRepository.GetById(image.Id);
                        if (fromShelf != null)
                        {
                            fromShelf.IsActive = false;
                        }
                        _productImagesRepository.Update(fromShelf);
                        await _unitOfWork.SaveAsync();
                    }
                }
              
                result = new WorkResultDto<Guid, GeneralWorkStatus>(productId, GeneralWorkStatus.Success);
                return result;
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProperty(Guid propertyId)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var fromShelf = _productPropertiesRespository.GetById(propertyId);
                if (fromShelf != null)
                {
                    fromShelf.IsActive = false;
                }
                _productPropertiesRespository.Update(fromShelf);
                await _unitOfWork.SaveAsync();
                result = new WorkResultDto<Guid, GeneralWorkStatus>(propertyId, GeneralWorkStatus.Success);
                return result;
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<List<Product>> SearchProduct(string id = "", string name = "", string anyPropertyName = "", string anyPropertyValue = "")
        {
            List<Product> products = new List<Product>();
            try
            {
                var prods = await _productRepository.GetAsync(p => p.IsActive,null, "ProductProperties");
                if(prods == null)
                {
                    return null;
                }

                products = prods.ToList();

                if (!string.IsNullOrWhiteSpace(id))
                {
                    products = products.Where(p => p.Id.Equals(id)).ToList();
                }
                if (!string.IsNullOrWhiteSpace(name))
                {
                    products = products.Where(p => p.Name.Contains(name)).ToList();
                }
                if (!string.IsNullOrWhiteSpace(anyPropertyName))
                {
                    products = products.Where(p => p.ProductProperties.Any(pn => pn.ProductPropertyName.Contains(anyPropertyName))).ToList();
                }
                if (!string.IsNullOrWhiteSpace(anyPropertyValue))
                {
                    products = products.Where(p => p.ProductProperties.Any(pv => pv.Value.Contains(anyPropertyValue))).ToList();
                }

                return products;
               
            }catch(Exception)
            {
                throw new Exception();
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateProduct(Product product)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var fromShelf = await _productRepository.GetAsync(p => p.Id == product.Id);
                var fromShelfProduct = fromShelf.FirstOrDefault();
                fromShelfProduct.Name = product.Name;
                fromShelfProduct.Description = product.Description;
                fromShelfProduct.Unit = product.Unit;
                //if(product.Category != null)
                //{
                //    fromShelfProduct.Category = new CategoryType();
                //    fromShelfProduct.Category.Id = product.Category.Id;
                //}
                _productRepository.Update(fromShelfProduct);
                await _unitOfWork.SaveAsync();
                return result = new WorkResultDto<Guid, GeneralWorkStatus>(product.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateProductProperty(ProductProperties productProperties)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var fromShelf = await _productPropertiesRespository.GetAsync(p => p.Id == productProperties.Id);
                var fromShelfProduct = fromShelf.FirstOrDefault();
                fromShelfProduct.Value = productProperties.Value;
                _productPropertiesRespository.Update(fromShelfProduct);
                await _unitOfWork.SaveAsync();
                return result = new WorkResultDto<Guid, GeneralWorkStatus>(productProperties.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }
    }
}
