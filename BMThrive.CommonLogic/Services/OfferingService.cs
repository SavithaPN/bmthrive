﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.DataAccess.Repository.Interfaces;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Services
{
    public class OfferingService : IOfferingService
    {
        IUnitOfWork _unitOfWork;
        IRepository<Offering> _offeringRepository;

        public OfferingService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _offeringRepository = _unitOfWork.GetRepository<Offering>();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddOffer(Offering offer)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _offeringRepository.Insert(offer);
                await _unitOfWork.SaveAsync();
                return result = new WorkResultDto<Guid, GeneralWorkStatus>(offer.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteOffer(Guid id)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var fromShelf = await _offeringRepository.GetAsync(o => o.Id == id);
                var fromShelOffering = fromShelf.FirstOrDefault();
                fromShelOffering.IsActive = false;
                _offeringRepository.Update(fromShelOffering);
                await _unitOfWork.SaveAsync();
                return result = new WorkResultDto<Guid, GeneralWorkStatus>(id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<List<Offering>> SearchOfferings(string searchText = "", bool includeExpiredOffer = false)
        {
            List<Offering> offerings = new List<Offering>();
            if (string.IsNullOrWhiteSpace(searchText))
            {
                var offers = await _offeringRepository.GetAsync(o => o.IsActive);
                offerings = offers.ToList();
            }
            else
            {
                var offer = await _offeringRepository.GetAsync(o => o.OfferDescription.Contains(searchText) && o.IsActive);
                offerings = offer.ToList();
            }

            if (!includeExpiredOffer)
            {
                offerings = offerings.Where(o => o.ExpiryDate <= DateTime.UtcNow).ToList();
            }

            return offerings;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateOffer(Offering offer)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var fromShelf = await _offeringRepository.GetAsync(o => o.Id == offer.Id);
                var fromShelOffering = fromShelf.FirstOrDefault();
                fromShelOffering.IsActive = offer.IsActive;
                fromShelOffering.OfferDescription = offer.OfferDescription;
                fromShelOffering.ExpiryDate = offer.ExpiryDate;
                fromShelOffering.PackageFormat = offer.PackageFormat;
                
                _offeringRepository.Update(fromShelOffering);
                await _unitOfWork.SaveAsync();
                return result = new WorkResultDto<Guid, GeneralWorkStatus>(offer.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }
    }
}
