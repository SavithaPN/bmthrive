﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.DataAccess.Repository.Interfaces;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Services
{
    public class GroupService : IGroupService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Group> _groupRepository;
        private readonly IRepository<GroupType> _groupTypeRepository;
        private readonly IRepository<MemberGroupMap> _memberGroupMapRepository;

        public GroupService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _groupRepository = _unitOfWork.GetRepository<Group>();
            _groupTypeRepository = _unitOfWork.GetRepository<GroupType>();
            _memberGroupMapRepository = _unitOfWork.GetRepository<MemberGroupMap>();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddGroup(Group group)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _groupRepository.Insert(group);
                await _unitOfWork.SaveAsync();
                return new WorkResultDto<Guid, GeneralWorkStatus>(group.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddMemberGroupMap(MemberGroupMap memberGroupMap)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _memberGroupMapRepository.Insert(memberGroupMap);
                await _unitOfWork.SaveAsync();
                return new WorkResultDto<Guid, GeneralWorkStatus>(memberGroupMap.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteGroup(Guid groupId)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _groupRepository.Delete(groupId);
                await _unitOfWork.SaveAsync();
                return new WorkResultDto<Guid, GeneralWorkStatus>(groupId, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteMemberGroupMap(Guid memberGroupMapId)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _memberGroupMapRepository.Delete(memberGroupMapId);
                await _unitOfWork.SaveAsync();
                return new WorkResultDto<Guid, GeneralWorkStatus>(memberGroupMapId, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<List<GroupType>> GetGroupTypes()
        {
            try
            {
                List<GroupType> groupTypes = new List<GroupType>();
                var res = await _groupTypeRepository.GetAsync();
                groupTypes = res.ToList();
                return groupTypes;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                return null;
            }
        }

        public async Task<List<Group>> ListAllGroups()
        {
            try
            {
                List<Group> groups = new List<Group>();
                var res = await _groupRepository.GetAsync();
                groups = res.ToList();
                return groups;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                return null;
            }
        }

        public async Task<List<Group>> ListGroups(Guid customerId)
        {
            try
            {
                List<Group> groups = new List<Group>();
                var res = await _groupRepository.GetAsync(g =>g.CustomerId == customerId);
                groups = res.ToList();
                return groups;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                return null;
            }
        }
    }
}
