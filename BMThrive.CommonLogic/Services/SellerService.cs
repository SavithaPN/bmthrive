﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.DataAccess.Repository.Interfaces;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Services
{
    public class SellerService : ISellerService
    {
        IUnitOfWork _unitOfWork;
        IRepository<Seller> _sellerRepository;

        public SellerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _sellerRepository = _unitOfWork.GetRepository<Seller>();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateSeller(Seller seller)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _sellerRepository.Insert(seller);
                await _unitOfWork.SaveAsync();
                return new WorkResultDto<Guid, GeneralWorkStatus>(seller.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> DeleteSeller(Guid id)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var fromShelf = await _sellerRepository.GetAsync(s=>s.Id == id);
                var fromShelfSeller = fromShelf.FirstOrDefault();
                fromShelfSeller.IsActive = false;
                await _unitOfWork.SaveAsync();
                return new WorkResultDto<Guid, GeneralWorkStatus>(id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<Seller> GetSeller(Guid id)
        {
            var seller = await _sellerRepository.GetAsync(s => s.Id == id && s.IsActive);
            return seller.FirstOrDefault();
        }

        public async Task<List<Seller>> SearchSeller(string address = "", string name = "")
        {
            IEnumerable<Seller> sellers = null;
            sellers = await _sellerRepository.GetAsync(s=>s.IsActive , null, "Address");
            if ( sellers != null && sellers.Count() > 0 && !string.IsNullOrWhiteSpace(address))
            {
                var sellersPresent = sellers.Where(s => s.Address.City.Contains(address) || s.Address.Country.Contains(address) || s.Address.ZipCode.Contains(address));
                sellers = sellersPresent.ToList();
            }

            if (sellers != null && sellers.Count() > 0  && !string.IsNullOrWhiteSpace(name))
            {
                sellers = sellers.Where(s => s.FirstName.Contains(name) || s.LastName.Contains(name));
            }

            return sellers.ToList();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> UpdateSeller(Seller seller)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var fromShelf = await _sellerRepository.GetAsync(s => s.Id == seller.Id);
                var fromShelfSeller = fromShelf.FirstOrDefault();
                fromShelfSeller.FirstName = seller.FirstName;
                fromShelfSeller.LastName = seller.LastName;
                fromShelfSeller.IsActive = seller.IsActive;
                fromShelfSeller.LicenseNumber = seller.LicenseNumber;
                await _unitOfWork.SaveAsync();
                return new WorkResultDto<Guid, GeneralWorkStatus>(seller.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }
    }
}
