﻿using BMThrive.Common.Dto;
using BMThrive.Common.Dto.Enum;
using BMThrive.CommonLogic.Interfaces;
using BMThrive.DataAccess.Repository.Interfaces;
using BMThrive.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMThrive.CommonLogic.Services
{
    public class ComponentService : IComponentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IRepository<Component> _componentRepository;
        private IRepository<ComponentImages> _componentImagesRepository;
        private IRepository<ComponentProperties> _componentPropertiesRepository;

        public ComponentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _componentRepository = unitOfWork.GetRepository<Component>();
            _componentPropertiesRepository = unitOfWork.GetRepository<ComponentProperties>();
            _componentImagesRepository = unitOfWork.GetRepository<ComponentImages>();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddComponentPicture(ComponentImages componentImage)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _componentImagesRepository.Insert(componentImage);
                await _unitOfWork.SaveAsync();
                return result = new WorkResultDto<Guid, GeneralWorkStatus>(componentImage.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddComponentProperty(ComponentProperties componentProperty)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _componentPropertiesRepository.Insert(componentProperty);
                await _unitOfWork.SaveAsync();
                return result = new WorkResultDto<Guid, GeneralWorkStatus>(componentProperty.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateComponentFull(Component component)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _componentRepository.Insert(component);
                await _unitOfWork.SaveAsync();
                return result = new WorkResultDto<Guid, GeneralWorkStatus>(component.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> CreateComponentLite(Component component)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                _componentRepository.Insert(component);
                await _unitOfWork.SaveAsync();
                return result = new WorkResultDto<Guid, GeneralWorkStatus>(component.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<Component> GetComponent(Guid id)
        {
            var component = await _componentRepository.GetAsync(c => c.Id == id);
            return component.FirstOrDefault();
        }

        public async Task<List<ComponentImages>> GetComponentImages(Guid componentId)
        {
            var componentImages = await _componentImagesRepository.GetAsync(c => c.ComponentId == componentId);
            return componentImages.ToList();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemovePicture(Guid imageId)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var fromShelf = await _componentImagesRepository.GetAsync(ci => ci.Id == imageId);
                var fromShelfImage = fromShelf.FirstOrDefault();
                fromShelfImage.IsActive = false;
                _componentImagesRepository.Update(fromShelfImage);
                await _unitOfWork.SaveAsync();
                return result = new WorkResultDto<Guid, GeneralWorkStatus>(imageId, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> RemoveProperty(Guid propertyId)
        {
            WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>();
            try
            {
                var fromShelf = await _componentPropertiesRepository.GetAsync(ci => ci.Id == propertyId);
                var fromShelfProperty = fromShelf.FirstOrDefault();
                fromShelfProperty.IsActive = false;
                _componentPropertiesRepository.Update(fromShelfProperty);
                await _unitOfWork.SaveAsync();
                return result = new WorkResultDto<Guid, GeneralWorkStatus>(propertyId, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(new ExceptionHelper().GetFullExceptionString(ex));
                return result;
            }
        }

        public async Task<List<Component>> SearchComponent(string searchText = "")
        {
            List<Component> componentsList = new List<Component>();
            var components = await _componentRepository.GetAsync(c => c.IsActive, null, "ComponentProperties");
            if (components == null)
            {
                return null;
            }

            componentsList = components.ToList();
            if (!string.IsNullOrWhiteSpace(searchText))
            {
                componentsList = componentsList.Where(c => c.Name.Contains(searchText) || c.Description.Contains(searchText)).ToList();
            }

            return componentsList;
        }
    }
}
